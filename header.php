<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta name="description" content"">
	<title>LOBO</title>
	<link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>	
	<link href="./css/style.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>	
	<link rel="stylesheet" href="./css/jquery-ui.css">	
	<link href="./css/bootstrap.css" rel="stylesheet"/>
	<script type="text/javascript" src="./js/jquery-1.11.1.js"></script>
	<script src="./js/jquery-ui.js"></script>	
	<script type="text/javascript" src="./js/bootstrap.js"></script>
	<script>
		  $(function() {
		    $( "#accordion" ).accordion({
		      heightStyle: "fill"
		    });
		  });
		  $(function() {
		    $( "#accordion-resizer" ).resizable({
		      minHeight: 140,
		      minWidth: 200,
		      resize: function() {
		        $( "#accordion" ).accordion( "refresh" );
		      }
		    });
		  });
  	</script>

</head>
<body>
	<div class="container">
			<div class="row show-grid">
				<div class="izq logo" class="hidden-xs hidden-sm col-md-5 col-lg-6">
					<img class="hidden-xs hidden-sm" src="./img/logo.png" alt="logotipo web" />
				</div>
				<div class="izq nombre" class="hidden-xs col-sm-3 col-md-5 col-lg-6">
					<img class="hidden-xs" src="./img/lobo.png" alt="logotipo web" />
				</div>
				<div class="col-xs-12 col-sm-7 col-md-6 col-lg-5">
					<nav class="navbar navbar-default" style="background-color:white">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#colapsar">
									<!--sr-only  clase a aplicar para que aparezca y desaparezca el menú que se despliega-->
									<span class="sr-only">Toggle</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<!--ponemos la marca de la web-->
								<a href="#" class="navbar-brand"><img class="hidden-sm hidden-md hidden-lg" src="./img/lobo_cabecero.png" alt="logotipo web" /></a>
							</div>
							<div class="collapse navbar-collapse" id="colapsar">
								<ul class="nav navbar-nav">
									<li><a href="./index.php"> Inicio </a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown"> Descargas APP <b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li>
												<a href="https://play.google.com/store?hl=es" target="_blank">Google Play</a>
											</li>
											<li>
												<a href="https://www.microsoft.com/es-es/store/apps/windows-phone" target="_blank">Windows Phone</a>
											</li>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tutoriales APP <b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li>
												<a href="#">Tutorial 1</a>
											</li>
											<li>
												<a href="#">Tutorial 2</a>
											</li>
										</ul>
									</li>
									<li><a href="contacto.php">Contacto</a></li>
								</ul>					
							</div>
					</nav>
				</div>
			</div>
	</div>
	<div class="separador hidden-xs">
				<div class="redessociales">	
					<a href="https://twitter.com/Copirally" class="img" target="_blank"><img src="./img/twitterlogo.png"/></a>
					<a href="https://plus.google.com/u/0/+Copirally/about" class="img" target="_blank"><img src="./img/googlepluslogo.png"/></a>
					<a href="https://www.youtube.com/user/CopiRallyCom" class="img" target="_blank"><img src="./img/youtubelogo.png"/></a>
					<a href="https://www.facebook.com/Copirally" class="img" target="_blank"><img src="./img/facebooklogo.png"/></a>
				</div>
	</div>