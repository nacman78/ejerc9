<?php
	include ("header.php");
?>

	<section class="clearfix">
		<div class="container nwd_fondo">
			<div class="row show-grid">
				<div class="hidden-xs col-sm-12 col-md-8 col-lg-8">
					<div id="slider">
						<div id="wowslider-container1">
							<div class="ws_images hidden-xs">
								<ul>
									<li><img src="data1/images/banner01.jpg" alt="foto 1" title="foto 1" id="wows1_0"/>paisaje 1</li>
									<li><img src="data1/images/banner02.jpg" alt="foto 2" title="foto 2" id="wows1_1"/>paisaje 2</li>
									<li><img src="data1/images/banner03.jpg" alt="foto 3" title="foto 3" id="wows1_2"/>paisaje 3</li>
									<li><img src="data1/images/copirally01.jpg" alt="foto 4" title="foto 4" id="wows1_3"/>rally1</li>
									<li><a href="http://wowslider.com/vf"><img src="data1/images/copirally02.jpg" alt="full screen slider" title="foto 5" id="wows1_4"/></a>rally 2</li>
									<li><img src="data1/images/copirally03.jpg" alt="foto 6" title="foto 6" id="wows1_5"/>rally3</li>
								</ul>
							</div>
							<div class="ws_bullets hidden-xs">
								<div>
									<a href="#" title="foto 1"><img src="data1/tooltips/banner01.jpg" alt="foto 1"/>1</a>
									<a href="#" title="foto 2"><img src="data1/tooltips/banner02.jpg" alt="foto 2"/>2</a>
									<a href="#" title="foto 3"><img src="data1/tooltips/banner03.jpg" alt="foto 3"/>3</a>
									<a href="#" title="foto 4"><img src="data1/tooltips/copirally01.jpg" alt="foto 4"/>4</a>
									<a href="#" title="foto 5"><img src="data1/tooltips/copirally02.jpg" alt="foto 5"/>5</a>
									<a href="#" title="foto 6"><img src="data1/tooltips/copirally03.jpg" alt="foto 6"/>6</a>
								</div>
							</div>
							<!--<span class="wsl"><a href="http://wowslider.com/vu">image carousel</a> by WOWSlider.com v6.4</span>-->
							<div class="ws_shadow"></div>
						</div>
						<script type="text/javascript" src="engine1/wowslider.js"></script>
						<script type="text/javascript" src="engine1/script.js"></script>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="sidebar">
						<div id="accordion">
						    <h3>¿Qué es "LOBO"</h3>
						    <div>
						      <p>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.</p>
						    </div>
						    <h3>¿para qué dispositivo es válido?</h3>
						    <div>
						      <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna. </p>
						    </div>
						    <h3>¿qué debo hacer para que funcione?3</h3>
						    <div>
						      <p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui. </p>
						      <ul>
						        <li>List item one</li>
						        <li>List item two</li>
						        <li>List item three</li>
						      </ul>
						    </div>
						    <h3>¿cuánto cuesta? / Totalmente GRATUITO</h3>
						    <div>
						      <p>Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia mauris vel est. </p><p>Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
						    </div>
			  			</div>
					</div>
				</div>
			</div>
		<!-- </div> -->
		
		<!-- <div class="container"> -->
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<article id="descarga">
					<p>Descarga nuestra aplicación móvil:</p>
					<a href="https://play.google.com/store?utm_source=emea_Med&utm_medium=hasem&utm_content=090114&utm_campaign=BKWS&pcampaignid=MKT-EG-emea-es-all-Med-hasem-py-BKWS-090114-1" target="_blank"><img class="botones img-responsive" src="./img/google_play.jpg"></a>
					<a href="http://www.windowsphone.com/es-es/store/featured-apps" target="_blank"><img class="botones img-responsive" src="./img/windows_phone.png"></a>

				</article>
				</div>	
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<article id="comenzar">
					Ejecuta la aplicación y disfruta de sus resultados:
					
					<button id="btn" class="nwd_boton">click</button>

				</article>
				</div>
			</div>
		</div>
	</section>

<?php
	include ("footer.php");
?>